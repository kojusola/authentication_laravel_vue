require('./bootstrap');

window.Vue = require('vue');
import BootstrapVue from 'bootstrap-vue' //Importing
import router from './router';

Vue.use(BootstrapVue);


const app = new Vue({
    el: '#app',
    router
});
