import Vue from 'vue'

import Router from 'vue-router';
Vue.use(Router);
import login from './components/login.vue'
import dashboard from './components/dashboard.vue'
import error from './components/notFound.vue'
const routes = [
    {
        path: "*",
        component:error
    },
    {
        path:'/',
        component:login,
        name:"login"
    },
    {
        path: "/dashboard",
        component:dashboard,
        name:"dashboard",
        // ensuring it is authenticated user is authenticated
        beforeEnter:(to, form, next)=>{
            axios.get('/api/authenticated').then(()=>{
                next()
            }).catch(()=>{
                return next({name:"login"})
            })
        }
    }
]

export default new Router({
    mode:'history',
    routes
})