<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\validation\ValidationException;

class loginController extends Controller
{
    // login and save user
    public function login (Request $request)
    {
        // validate input
        $request->validate([
            'email' =>['required', 'email'],
            'password' =>['required', 'min:6']
        ]);
        // login existing user
        if(Auth::attempt($request -> only('email','password'))){
            return response()->json(Auth::user(), 200);
        }else if(User::where('email', '=', $request['email'])->count() > 0){
            // if user existing and credentials wrong, give error message
            throw ValidationException:: withMessages([
                'credentials' => ['The provided credentials are incorrect']
            ]);
        } else{
            // new user create account and save user in database
            user::create([
                'email'=>$request -> email,
                'password' => Hash::make($request->password)
            ]);
            // login user
            Auth::attempt($request -> only('email','password'));
            return response()->json(Auth::user(), 200);
        }
    }
    //logout user
    public function logout (){
        Auth::logout();
    }
}
