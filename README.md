This is a simple authentication(log in and log out) web app using laravel and vue;

To install dependencies :
1. npm dependencies: npm install
2. composer dependencies: composer install

To run the app at localhost:8000 :
laravel : php artisan serve;
vue : npm run watch;




